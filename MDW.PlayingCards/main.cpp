// Playing Cards
// Max Wilke


#include <iostream>
#include <conio.h>

using namespace std;

//Abbie's Playing Cards -- forked from Max

enum Suit
{
	Hearts,
	Spades,
	Diamonds,
	Clubs
};

const char* GetSuitName(enum Suit);
const char* GetRankName(enum Rank);

enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Suit Suit;
	Rank Rank;
};

void PrintCard(Card card) 
{
	cout << GetRankName(card.Rank) << " of " << GetSuitName(card.Suit);
}

Card HighCard(Card card1, Card card2) 
{
	
	if (card1.Rank > card2.Rank) 
	{
		PrintCard(card1);
	}
	else if (card2.Rank > card1.Rank) 
	{
		PrintCard(card2);
	}
	else 
	{
		cout << "The two cards are of equal value";
	}
	return card1;
}

int main()
{
	Card c1;
	c1.Rank = Two;
	c1.Suit = Diamonds;

	Card c2;
	c2.Rank = Ace;
	c2.Suit = Clubs;

	HighCard(c1, c2);

	(void)_getch();
	return 0;
}
const char* GetRankName(enum Rank rank) 
{
	switch (rank) {
	case Two: return "Two";
	case Three: return "Three";
	case Four: return "Four";
	case Five: return "Five";
	case Six: return "Six";
	case Seven: return "Seven";
	case Eight: return "Eight";
	case Nine: return "Nine";
	case Ten: return "Ten";
	case Jack: return "Jack";
	case Queen: return "Queen";
	case King: return "King";
	}
}

const char* GetSuitName(enum Suit suit) 
{
	switch (suit) {
	case Hearts: return "Hearts";
	case Spades: return "Spades";
	case Diamonds: return "Diamonds";
	case Clubs: return "Clubs";
	}
}